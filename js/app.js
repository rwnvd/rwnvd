//navbar
const navSlide = ()=>{
    const burger= document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const navLinks = document.querySelectorAll('.nav-links li');

    burger.addEventListener('click', ()=>{
        nav.classList.toggle('nav-active');

        navLinks.forEach((link, index) => {
            if(link.style.animation){
                link.style.animation = '';
            }else{
                link.style.animation = `navLinksFade 0.5s ease forwards ${index / 6 + 0.2}s`
            }
        });

        burger.classList.toggle('toggle');

    });
}

navSlide();


//slidein
$(window).scroll(function() {
    $(".slideanim").each(function(){
        var pos = $(this).offset().top;
    
        var winTop = $(window).scrollTop();
        if (pos < winTop + 850) {
        $(this).addClass("slide");
        }
    });
});

//indicator
window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

//scroll links
$('.scroll-start').click (function() {
    $('html, body').animate({scrollTop: $('section.start').offset().top }, 'slow');
    return false;
  });

$('.scroll-middle').click (function() {
    $('html, body').animate({scrollTop: $('section.middle').offset().top }, 'slow');
    return false;
});

$('.scroll-end').click (function() {
  $('html, body').animate({scrollTop: $('section.end').offset().top }, 'slow');
  return false;
});

//slide in intro
const tl = gsap.timeline({defaults: {ease: 'power1.out'}});

tl.to('.text', {y:"0%" , duration: 1.1, stagger: 0.5 });
tl.to('.slider', {y:"-100%", duration: 1.5, delay: 0.5});
tl.to('.intro' , {y:"-100%", duration: 1} , "-=1");
tl.fromTo('nav', {opacity: 0} , {opacity: 1 , duration: 1 });


//swup
const options = {
    linkSelector:
      'a[href^="' +
      window.location.origin +
      '"]:not([data-no-swup]), a[href^="/"]:not([data-no-swup]), a[href^="#"]:not([data-no-swup])'
  };
const swup = new Swup(options);
